import { apiUrl } from "@/core/constants";
import axios from "axios";

export const getInputAxios = async () => {
  try {
    const response = await axios.get(apiUrl);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}