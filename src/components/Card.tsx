import { Card } from '@mui/material'
import Resume from './Resume';
import styles from '@/styles/Home.module.css'
import { ResumeI } from '@/models/Resume';
import Form from './Form';

const CardCenter = ({isFlipped,dynamicForm,handleConfirmClick,formValues,handleChange,showError,setIsFlipped}:ResumeI) => {
    return (
        <Card className={`${styles.card} ${isFlipped ? styles.flip : ''}`} style={{ backgroundColor: "rgba(26, 20, 20, 0.836)", padding: "25px", borderRadius: "5%" }}>
        { isFlipped ? 
        <Resume setIsFlipped={setIsFlipped} formValues={formValues} dynamicForm={dynamicForm}></Resume> 
        : 
        <Form handleConfirmClick={handleConfirmClick} dynamicForm={dynamicForm} formValues={formValues} handleChange={handleChange} showError={showError}></Form>
        }
        </Card>
    );
}
export default CardCenter
