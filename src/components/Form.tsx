import { Button, Grid } from "@mui/material";
import RenderInput from "./RenderInput";
import { RenderInputProps } from "@/models/RenderInputProps";
import { margin } from "@/core/constants";

const Form = ({dynamicForm, formValues, handleChange,showError, handleConfirmClick}: RenderInputProps): JSX.Element => {
    return (
      <Grid container spacing={2} style={{ marginTop: margin }}>
        <RenderInput dynamicForm={dynamicForm} showError={showError} formValues={formValues} handleChange={handleChange}></RenderInput>
        <Grid item xs={12} >
          <Button style={{ borderColor: "white", border: "1px solid white", color: "gray", backgroundColor: "rgba(17, 13, 13, 0.836)" }} fullWidth variant='contained' onClick={handleConfirmClick}>Confirmar</Button>
        </Grid>
      </Grid>
    )
}
export default Form;