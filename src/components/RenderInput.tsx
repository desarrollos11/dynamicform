import { Item } from '@/models/Item';
import { RenderInputProps } from '@/models/RenderInputProps';
import { FormControlLabel, Grid, MenuItem, Radio, RadioGroup, Select, TextField } from '@mui/material'

const RenderInput = ({ dynamicForm, formValues, handleChange,showError }: RenderInputProps) => {
    const renderInputs = () => {
        return dynamicForm.map((item:Item) => {
          const { name } = item;
          const value = formValues[name] || "";
      
          return (
            <Grid item xs={12} key={name}>
              {renderInputByType(item, value)}
            </Grid>
    
          );
        });
      };
      
      const renderInputByType = (item:Item, value="") => {
        const { label, name, isRequired, disabled, type, items } = item;
        switch (type) {
          case 'TextInput':
            return (
              <TextField
              sx={{ input: { color:  disabled ? 'gray' : 'white' } }}
              InputLabelProps={{ style: { color:  disabled ? 'gray' : 'white'  } }}
              style={{borderColor:"white",border:"1px solid white",color:"white", backgroundColor: "rgba(17, 13, 13, 0.836)"}}
              label={label}
                name={name}
                error={showError[name]}
                required={isRequired}
                disabled={disabled}
                value={value}
                onChange={(e) => handleChange!!(name, e.target.value)}
                fullWidth
              />
            );
      
          case 'Select':
            return (
              <Select
                style={{borderColor:"white",border:"1px solid white",color:  disabled ? 'gray' : 'white' , backgroundColor: "rgba(17, 13, 13, 0.836)"}}
                inputProps={{ style: { color: "white" } }}
                label={label}
                value={value == "" ? "0" : value}
                name={name}
                required={isRequired}
                disabled={disabled}
                onChange={(e) => handleChange!!(name, e.target.value)}
                fullWidth
              >
                <MenuItem value={"0"}>Seleccione una opción</MenuItem>
                {items.map((option:any, index) => (
                  <MenuItem key={index} value={option.value}>
                    {option.value}
                  </MenuItem>
                ))}
              </Select>
            );
      
          case 'Radio':
            return (
              <RadioGroup
                name={name}
                value={value}
                color='primary'
                style={{padding: '5px', color:  disabled ? 'gray' : 'white' }}
                onChange={(e) => handleChange!!(name, e.target.value)}
              >
                {items.map((radioItem:any) => (
                  <FormControlLabel
                  
                    key={radioItem.id}
                    value={radioItem.value}
                    control={<Radio disabled={disabled} />}
                    label={radioItem.value}
                  />
                ))}
              </RadioGroup>
            );
            
          default:
            return null;
        }
      };

      return (renderInputs())
}


export default RenderInput;