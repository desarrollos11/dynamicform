import { margin } from "@/core/constants";
import { Item } from "@/models/Item";
import { RenderInputProps } from "@/models/RenderInputProps";
import { getColorValue } from "@/utils/getColorValue";
import { Grid, Typography } from "@mui/material";

const RenderResume = ({formValues, dynamicForm} : RenderInputProps) => {
    let colorValueResume = "white";
    const stringRequired = "Debe Rellenar el Campo";

    return dynamicForm.map((item: Item, index) => {
      const { label, name } = item
      let value = formValues[name] || stringRequired
      value = (value === "0") ? stringRequired : value;
      const colorValueResume = getColorValue(value, stringRequired);
      return (
        <Grid key={index} container spacing={2} style={{ marginTop: margin }}>
          <Grid item xs={6}>
            <Typography style={{ color: "white" }}>{label}:</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography style={{ color: colorValueResume, marginLeft: "25px" }}>{value}</Typography>
          </Grid>
        </Grid>
      )
    })
  }
  export default RenderResume;