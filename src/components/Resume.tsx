import { margin } from "@/core/constants";
import { Button, Grid } from "@mui/material";
import { Dispatch, SetStateAction } from "react";
import RenderResume from "./RenderResume";
import { RenderInputProps } from "@/models/RenderInputProps";
import { ResumeI } from "@/models/Resume";


const Resume = ({setIsFlipped, formValues, dynamicForm}:ResumeI) => {
    return ( 
      <Grid container spacing={2} style={{ marginTop: margin }}>
        <Grid item xs={12} >
          <RenderResume formValues={formValues} dynamicForm={dynamicForm}></RenderResume>
        </Grid>
        <Grid item xs={12} >
          <Button style={{ borderColor: "white", marginTop:"25px", marginBottom:margin, border: "1px solid white", color: "gray", backgroundColor: "rgba(17, 13, 13, 0.836)" }} fullWidth variant='contained' onClick={() => setIsFlipped!!(false)}>Volver</Button>
        </Grid>
      </Grid>
    )
}
export default Resume;