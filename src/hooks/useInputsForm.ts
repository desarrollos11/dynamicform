import { useEffect, useState } from 'react'
import { getInputAxios } from '@/api/getInputAxios';

export const useInputsForm = () => {
    const [dynamicForm, setDynamicForm] = useState([]);
    const [formValues, setFormValues] = useState({});
    const [showError, setShowError] = useState({});
    const [isFlipped, setIsFlipped] = useState(false);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        getInputsForm()
    }, [])

    const handleConfirmClick = () => {
        onClickConfirm();
        setIsFlipped(true);
    }
    const onClickConfirm =() => {
        validateForm();
    }
    const validateForm = () => {
        const errors: {[key: string]: boolean} = {};
        dynamicForm.forEach((item: any) => {
            if (!formValues[item.name as keyof typeof formValues] && item.isRequired) {
                errors[item.name] = true;
            }
        });
        setShowError(errors);
    }
    const getInputsForm = async () => {
        const response = await getInputAxios();
        setDynamicForm(response);
        setIsLoading(false);
    }
      // Manejador de cambios para los inputs
    const handleChange = (name:string, value:string) => {
        setFormValues((prevValues) => ({
        ...prevValues,
        [name]: value,
        }));
    };
    return {
        dynamicForm,
        getInputsForm,
        handleChange,
        formValues,
        handleConfirmClick,
        showError,
        isFlipped,
        setIsFlipped,
        isLoading
    }
}
