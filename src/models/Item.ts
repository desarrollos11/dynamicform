export interface Item {
    label:string,
    name:string,
    isRequired:boolean,
    disabled:boolean,
    type:string,
    items:Item[],
    validation:string
  }