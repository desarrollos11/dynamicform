export interface RenderInputProps {
    dynamicForm: any[]; 
    formValues: any; 
    handleChange?: (name: string, value: string) => void; 
    showError?:any;
    handleConfirmClick?: () => void; 

}