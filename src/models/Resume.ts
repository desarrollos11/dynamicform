import { Dispatch, SetStateAction } from "react";
import { RenderInputProps } from "./RenderInputProps";

export interface ResumeI extends RenderInputProps {
    isFlipped?:boolean;
    setIsFlipped?: Dispatch<SetStateAction<boolean>>;
}