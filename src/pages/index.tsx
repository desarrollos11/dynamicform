import styles from '@/styles/Home.module.css'
import { CircularProgress } from '@mui/material'
import { useInputsForm } from '@/hooks/useInputsForm'
import CardCenter from '@/components/Card'

export default function Home() {
  const { dynamicForm, handleChange, formValues, handleConfirmClick, showError, isFlipped, setIsFlipped, isLoading } = useInputsForm()

  return (
    <div className={styles.main}>
      {isLoading ?
        <CircularProgress size={150} />
        :
        <CardCenter dynamicForm={dynamicForm} handleConfirmClick={handleConfirmClick} formValues={formValues} handleChange={handleChange} showError={showError} isFlipped={isFlipped} setIsFlipped={setIsFlipped} ></CardCenter>
      }
    </div>

  );
}
